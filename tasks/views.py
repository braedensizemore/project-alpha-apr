from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task

# Create your views here.


@login_required
def create_new_task(request):
    form = TaskForm()
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_user_tasks(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/list.html", context)
