from django.urls import path
from tasks.views import create_new_task, show_user_tasks

urlpatterns = [
    path("create/", create_new_task, name="create_task"),
    path("mine/", show_user_tasks, name="show_my_tasks"),
]
