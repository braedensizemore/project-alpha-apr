from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm

# Create your views here.


@login_required
def project_view(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def project_detail(request, pk):
    show_project = Project.objects.get(id=pk)
    context = {
        "show_project": show_project,
    }
    return render(request, "projects/show_projects.html", context)


@login_required
def create_project(request):
    form = ProjectForm()
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
